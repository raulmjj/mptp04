#RAUL ALBERTO SUAREZ
#DNI:33560394
import os
#Modulo
def menu():
    os.system('cls')
    print("1) Registrar productos")
    print("2) Mostrar el listado de productos")
    print("3) Mostrar los productos cuyo stock se encuentre en el intervalo [desde, hasta]")
    print("4) Agragar una cantidad de stock a los valores menores de stock que solicite")
    print("5) Eliminar todos los productos cuyo stock sea igual a cero.")
    print('6) SALIR...')
    opcion=int(input("Ingrese una opcion: "))
    while not (opcion>=1 and opcion<=6):
        opcion=int(input("Ingrese una opcion: "))
    return opcion

def leerProductos():
    print("### Cargar Lista de Productos ###")
    productos={}
    continuar="s"
    while continuar=="s":
        codigo=int(input("Ingrese Codigo: "))
        if codigo not in productos:
            descripcion=input("Descripcion: ")
            precio=leerPrecio()
            stock=leerStock()
            productos[codigo] = [descripcion,precio,stock] 
            print("¡¡¡Agregado Correctamente!!!")
        else:
            print("¡¡¡El Producto ya Existe!!!")    
        continuar=input("Desea Contniuar s/n: ")          
    return productos

def leerPrecio():
    precio=float(input("Ingrese Precio: "))
    while not(precio>=0):
        precio=float(input("Ingrese Precio: "))
    return precio

def leerStock():
    stock=int(input("Ingrese Stock: "))
    while not(stock>=0):
        stock=int(input("Ingrese Stock: "))
    return stock

def mostrar(productos):
    print("### Listado de Productos ###")
    for clave,valor in productos.items():
        print(clave,valor)

def pulsarTecla():
    input('Pulse un tecla...')  

def mostrarStock(productos):
    desde=int(input("Ingrese Stock Desde: "))
    hasta=leerHasta(desde)
    for clave,valor in productos.items():
        if productos[clave][2]>=desde and productos[clave][2]<=hasta:
            print(clave,valor)

def leerHasta(desde):
    hasta=int(input("Ingrese Stock Hasta: "))
    while not (hasta>=desde):
        hasta=int(input("Ingrese Stock Hasta: "))
    return hasta

def agregarStock(productos):
    agregarStock= int(input("¿Cuanto stock desea agregar?: "))
    menorStock= int(input("¿Valores de Stock Menores que?: "))
    for clave,valor in productos.items():
        if productos[clave][2]<menorStock:
            productos[clave][2]=productos[clave][2]+agregarStock
            print("Stock Agregado al Producto ",clave,valor)
            
def eliminarCeros(productos):
    print("¡¡¡Eliminando productos con Stock 0...!!!")
    i=1
    while i<=len(productos):
        for clave,valor in productos.items():
            if valor[2]==0 :
                print("Se Eliminara el Producto: ",productos[clave])
                del productos[clave]
                print("Eliminado...")
                break
        i+=1
    print("Lista de Productos Actualizada ", productos)

#Principal
opcion = 0
os.system('cls')
while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos=leerProductos()
        pulsarTecla()
    elif opcion==2:
        mostrar(productos)    
        pulsarTecla()
    elif opcion==3:
        mostrarStock(productos)
        pulsarTecla()
    elif opcion==4:
        agregarStock(productos)
        pulsarTecla()
    elif opcion==5:
        eliminarCeros(productos)    
        pulsarTecla()   
    elif opcion==6:
        print("FIN DEL PROGRAMA...")