#RAUL ALBERTO SUAREZ
#DNI:33560394 
import os
from io import open
from operator import itemgetter, attrgetter
#from typing import ItemsView
#Modulo
def menu():
    os.system('cls')
    print("1) Cargar Autos")
    print("2) Buscar un automóvil por su dominio utilizando la búsqueda binaria")
    print("3) Buscar un automóvil por su marca")
    print("4) Que auto desea guardar por su marca?")
    print("5) SALIR...")
    opcion=int(input("Ingrese una opcion: "))
    while not (opcion>=1 and opcion<=5):
        opcion=int(input("Ingrese una opcion: "))
    return opcion

def cargarLista():
    automovil=[]
    fichero = open('vehiculos.txt','r')
    texto = fichero.readlines()
    fichero.close()
    print(texto)
    for linea in texto:
        auto = linea.split(";")
        print(auto)
        auto[7] = auto[7].replace("\n", "")
        if (validarDominio(automovil,auto[0]) and validarMarca(auto[1]) and validarTipo(auto[2]) and validarModelo(int(auto[3])) and validarEstado(auto[7])):
            automovil.append([auto[0],auto[1],auto[2],int(auto[3]),int(auto[4]),int(auto[5]),int(auto[6]), auto[7]])
            print('Agregado correctamente!!!')
    return automovil

def validarDominio(automovil,dominio):
    valido = True
    for item in automovil:
        if dominio == item[0] or  not((len(dominio)>=6 and len(dominio)<=9)):
            valido = False
            break
    return valido 
    
def validarMarca(marca):
    valido = True
    if not(marca =='Renault' or marca =='Ford' or marca =='Citroen' or marca =='Fiat'):
        valido = False
    return valido

def validarTipo(tipo):
    valido = True
    if not(tipo == 'Automovil' or tipo == 'Utilitario'):  
        valido = False
    return valido

def validarModelo(modelo):
    valido = True
    if not(modelo>=2005 and modelo<=2020):
        valido = False
    return valido

def validarEstado(estado): # ■ Estado: (Vendido, Disponible, Reservado).
    valido = True
    if not(estado == 'Vendido' or estado == 'Disponible' or estado == 'Reservado'):
    #if not(estado=='Disponible'):  #'D\n'
        valido = False
    return valido

def pulsarTecla():
    input('Pulse un tecla...') 

def buscarDominio(automovil):
    dominio = input("¿Valor DOMINIO buscado?: ")
    listaOrdenada = sorted(automovil,key=itemgetter(0))
    print(listaOrdenada)
    resultado = busquedaBinaria(listaOrdenada, dominio)
    print(resultado)
    print ("Resultado:", listaOrdenada[resultado])

def busquedaBinaria(lista, x):
    izq = 0 # izq guarda el índice inicio del segmento
    der = len(lista) -1 # der guarda el índice fin del segmento
    # un segmento es vacío cuando izq > der:
    while izq <= der:
        medio = (izq+der)//2
        if lista[medio][0] == x:
            return medio
        elif lista[medio][0]> x:
            der = medio-1
        else:
            izq = medio+1
    return -1
def cambiarMayuscula(automovil):
    listaAuxiliar= []
    for item in automovil:
        listaAuxiliar.append([item[0],item[1].upper(),item[3],item[4],item[5],item[6],item[7]])    
    return listaAuxiliar

def buscarMarca(automovil):
    listaAuxiliar=cambiarMayuscula(automovil)
    marca = input("¿Que auto desea buscar?#MAYUSCULA#: ")
    for item in listaAuxiliar:
        posicion = item[1].find(marca)
        if posicion != -1:
          print(item)

def crearFichero(automovil):
    marca = input("¿Que auto desea filtrar y guardar?: ")
    listaMarca=[]
    for auto in automovil:
       if auto[1] == marca:
        listaMarca.append([auto[0],auto[1],auto[2],str(auto[3]),str(auto[4]),str(auto[5]),str(auto[6]), auto[7]])
    print(listaMarca)   
    for linea in listaMarca:
        texto = ';'.join(linea)
      
    nomFichero=marca+'.txt'
   
    fichAuto = open(nomFichero,'a+')
    for linea in listaMarca:
        texto = ';'.join(linea)
        fichAuto.write(texto+'\n')
        print(texto)
    fichAuto.close()
#Principal
opcion = 0
automovil=[]
os.system('cls')
while (opcion != 4):
    opcion = menu()
    if opcion == 1:
        automovil=cargarLista()
        for item in automovil:
          print(item)
        pulsarTecla()
    elif opcion==2:
        buscarDominio(automovil)
        pulsarTecla()      
    elif opcion==3:
        buscarMarca(automovil)
        pulsarTecla()
    elif opcion==4:
        crearFichero(automovil)
        pulsarTecla()
    elif opcion==5:
           print("FIN DEL PROGRAMA...") 
           pulsarTecla()     